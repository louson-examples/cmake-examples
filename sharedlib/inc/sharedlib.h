#ifndef _SHAREDLIB_H_
#define _SHAREDLIB_H_

void shared_init(void);
void shared_func(void);
void shared_term(void);

#endif  // _SHAREDLIB_H_
