cmake_minimum_required(VERSION 3.16)
project(myexe VERSION 1.0 LANGUAGES C)

file(GLOB sources src/*.c)
include_directories(inc)

add_executable(${PROJECT_NAME}  ${sources})

target_link_libraries(${PROJECT_NAME} sharedlib)

install(TARGETS ${PROJECT_NAME} DESTINATION lib)
install(FILES inc/sharedlib.h DESTINATION include)
